import { TodoAppV2Page } from './app.po';

describe('todo-app-v2 App', () => {
  let page: TodoAppV2Page;

  beforeEach(() => {
    page = new TodoAppV2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
